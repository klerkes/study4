# README #

This repository contains gaze data recorded for experiments reported in the PhD dissertation

#### Glimpsed. Improving natural language processing with gaze data

By Sigrid Klerke, University of Copenhagen, 2016

### What is this repository for? ###

* Documentation, replicability and future research.
* If you use it, please cite: Klerke Sigrid, Glimpsed. Improving natural language processing with gaze data. PhD dissertation (submitted), University of Copenhagen 2016.

### Data Format ###
The tabulated data represents one fixation per row.
Columns:

- **ParticipantName**:  Individual readers
- **MediaName**: Individual stimuli, incl. sentence type (original, simplified, etc.)
- **FixationIndex**: Individual fixations (resets per reader per stimulus)
- **RecordingTimeStamp**: Time stamp of fixation start (unique per RecordingName)
- **EyeTrackerTimeStamp**: Time stamp of fixation start (unique per RecordingName)
- **GazeEventType**: Only fixations are kept (unclassified gaze samples and saccades were deleted)
- **GazeEventDuration**: Duration of current fixation in ms
- **FixationPointX_MCSpx**: Estimated fixation-coordinate on screen in pixels, left to right
- **FixationPointY_MCSpx**: Estimated fixation-coordinate on screen in pixels, top to bottom
- **tID**: Unique Tweet ID
- **A**: Level of factor (amount of complex words)
- **C**: Level of factor (word length)
- **T**: Level of factor (with or without Twitter markup)
- **P**: Level of factor (spelling surprisal level)
- **factors**: Concatenation of factor levels
- **wID**: Index of currently fixated word (0-indexing)
- **word**: Currently fixated word

### Contact ###
If you have questions or use this dataset, please let me know.

* Sigrid Klerke (@ gmail.com)
* [Link to dissertation TBA]